create table oauth_client_details (
    client_id varchar(256),
    resource_ids varchar(256),
    client_secret varchar(256),
    scope varchar(256),
    authorized_grant_types varchar(256),
    web_server_redirect_uri varchar(256),
    authorities varchar(256),
    access_token_validity integer,
    refresh_token_validity integer,
    autoapprove varchar(256),
    additional_information varchar(256),
    primary key(client_id)
);

create table roles (
    id varchar(255),
    role varchar(255),
    primary key(id)
);

create table users (
    username character varying(255),
    password character varying(255),
    enabled boolean,
    primary key(username)
);

create table user_role (
    username character varying(255),
    id_role character varying(255),
    primary key(username, id_role)
);

insert into oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types,
    web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, autoapprove, additional_information)
values('ketapangclient', 'ketapangresource', 'ketapangsecret', 'write', 'implicit', '', 'CLIENT', 300, 300, 'true', null);