package lab.aikibo.oauthketapang.repo;

import lab.aikibo.oauthketapang.entity.OauthClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OauthClientDetailsRepo extends JpaRepository<OauthClientDetails, String> {
}
