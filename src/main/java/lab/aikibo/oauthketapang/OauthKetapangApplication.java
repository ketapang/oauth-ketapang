package lab.aikibo.oauthketapang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthKetapangApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthKetapangApplication.class, args);
	}
}
