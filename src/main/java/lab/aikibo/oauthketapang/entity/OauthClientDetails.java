package lab.aikibo.oauthketapang.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity @Getter @Setter
public class OauthClientDetails {

    @Id private String clientId;
    @Column private String resourceIds;
    @Column private String clientSecret;
    @Column private String scope;
    @Column private String authorizedGrantTypes;
    @Column private String webServerRedirectUri;
    @Column private String authorities;
    @Column private Integer accessTokenValidity;
    @Column private Integer refreshTokenValidity;
    @Column private String autoapprove;
    @Column private String additionalInformation;

}
